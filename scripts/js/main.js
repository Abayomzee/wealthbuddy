
$(document).ready(function(){

    $('#basic_input').click(function(){
        $("#verify_inputs_container").fadeOut(300);
        $("#basic_inputs_container").delay(300).fadeIn();
    })

    $('#verify_inut').click(function(){

        $('')

        $("#basic_inputs_container").fadeOut(300);
        $("#verify_inputs_container").delay(300).fadeIn();

    }) 

    $('#btn_verify_next').click(function(){
        $("#basic_inputs_container").fadeOut(300);
        $("#verify_inputs_container").delay(300).fadeIn();
    })

    $("#create_form_options li").click(function(){

        $("#create_form_options li.active").removeClass("active");
        $(this).addClass("active");

    })

    $('#btn_verify_next').click(function(e){
        e.preventDefault()

        $("#create_form_options li:nth-child(1)").removeClass("active");
        $("#create_form_options li:nth-child(2)").addClass("active");
    })

    $('#verify_data').click(function(){
        $('#page_content_wrapper').fadeOut(200);
        $(".success_msg_main_container").delay(200).fadeIn();
    })

    $('.sideNavBtnOpen').click(function(){
        $('.mobile_nav_div_container').css('width','100%');
    })

    $('.close_nav_btn').click(function(){
        $('.mobile_nav_div_container').css('width','0');
    })

    $('.dashboard_sideNav_open button').click(function(){
        $('.dashboard_mobile_sideNav').css('width','100%');
    })

    $('.dash_close_nav_btn').click(function(){
        $('.dashboard_mobile_sideNav').css('width','0');
    })

    var container_width = 260 * $(".each_type_of_goals").length;
   $(".type_of_goals").css("width", container_width+"px");







   $(".step_number div").click(function(){

    $(".step_number div.active").removeClass("active");
    $(this).addClass("active"); 

})


   $('#target_save_next_one').click(function(e){
    e.preventDefault()

    $(".step_number .step_number_one").removeClass("active");
    $(".step_number .step_number_two").addClass("active");

    $(".savings_details").fadeOut(300);
    $(".funding_source").delay(300).fadeIn();
})

$('#target_save_next_two').click(function(e){
    e.preventDefault()

    $(".step_number .step_number_two").removeClass("active");
    $(".step_number .step_number_three").addClass("active");

    $(".funding_source").fadeOut(300);
    $(".destination_account").delay(300).fadeIn();
})


$('#target_save_next_three').click(function(e){
    e.preventDefault()

    $(".step_number .step_number_three").removeClass("active");
    $(".step_number .step_number_four").addClass("active");

    $(".destination_account").fadeOut(300);
    $(".almost_done").delay(300).fadeIn();
})


$('.step_number_one').click(function(){

    $("[id*='psavings_']").not("#psavings_savings_details").fadeOut(300);

    $("#psavings_savings_details").delay(300).fadeIn();  
})

$('.step_number_two').click(function(){

    $("[id*='psavings_']").not("#psavings_funding_source").fadeOut(300);

    $("#psavings_funding_source").delay(300).fadeIn();  
})

$('.step_number_three').click(function(){

    $("[id*='psavings_']").not("#psavings_destination_account").fadeOut(300);

    $("#psavings_destination_account").delay(300).fadeIn();  
})

$('.step_number_four').click(function(){

    $("[id*='psavings_']").not("#psavings_almost_done").fadeOut(300);

    $("#psavings_almost_done").delay(300).fadeIn();  
})


$('#review_btn').click(function(){
    $('.gp_sav').fadeOut(300);
    $('.gp_sav_review').delay(300).fadeIn();
})

$('.filter_dropdown').click(function(){
    $('.second_child_filter_dropdown').fadeToggle();
})

$('.notification_div').click(function(e){
    e.preventDefault()

    $(".notification_div").removeClass("active_notification");
    $(this).addClass("active_notification");

    
})

// Questionnaire

$('#to_question_two').click(function(e){
    e.preventDefault();

    $(".question_1").fadeOut(300);
    $(".question_2").delay(300).fadeIn();

    $('head').append('<style>.question_main_wrapper:after{width: 25% !important;}</style>');

    

})

$('#back_to_question_one').click(function(e){
    e.preventDefault();

    $(".question_2").fadeOut(300);
    $(".question_1").delay(300).fadeIn();

    $('head').append('<style>.question_main_wrapper:after{width: 12.5% !important;}</style>');

})

$('#to_question_three').click(function(e){
    e.preventDefault();

    $(".question_2").fadeOut(300);
    $(".question_3").delay(300).fadeIn();

    $('head').append('<style>.question_main_wrapper:after{width: 37.5% !important;}</style>');

})

$('#back_to_question_two').click(function(e){
    e.preventDefault();

    $(".question_3").fadeOut(300);
    $(".question_2").delay(300).fadeIn();

    $('head').append('<style>.question_main_wrapper:after{width: 25% !important;}</style>');

})



$('#to_question_four').click(function(e){
    e.preventDefault();

    $(".question_3").fadeOut(300);
    $(".question_4").delay(300).fadeIn();

    $('head').append('<style>.question_main_wrapper:after{width: 50% !important;}</style>');

})

$('#back_to_question_three').click(function(e){
    e.preventDefault();

    $(".question_4").fadeOut(300);
    $(".question_3").delay(300).fadeIn();

    $('head').append('<style>.question_main_wrapper:after{width: 37.5% !important;}</style>');

})


$('#to_question_five').click(function(e){
    e.preventDefault();

    $(".question_4").fadeOut(300);
    $(".question_5").delay(300).fadeIn();

    $('head').append('<style>.question_main_wrapper:after{width: 62.5% !important;}</style>');

})

$('#back_to_question_four').click(function(e){
    e.preventDefault();

    $(".question_5").fadeOut(300);
    $(".question_4").delay(300).fadeIn();

    $('head').append('<style>.question_main_wrapper:after{width: 50% !important;}</style>');

})

$('#to_question_six').click(function(e){
    e.preventDefault();

    $(".question_5").fadeOut(300);
    $(".question_6").delay(300).fadeIn();

    $('head').append('<style>.question_main_wrapper:after{width: 75% !important;}</style>');

})

$('#back_to_question_five').click(function(e){
    e.preventDefault();

    $(".question_6").fadeOut(300);
    $(".question_5").delay(300).fadeIn();

    $('head').append('<style>.question_main_wrapper:after{width: 62.5% !important;}</style>');

})

$('#to_question_seven').click(function(e){
    e.preventDefault();

    $(".question_6").fadeOut(300);
    $(".question_7").delay(300).fadeIn();

    $('head').append('<style>.question_main_wrapper:after{width: 87.5% !important;}</style>');

})

$('#back_to_question_six').click(function(e){
    e.preventDefault();

    $(".question_7").fadeOut(300);
    $(".question_6").delay(300).fadeIn();

    $('head').append('<style>.question_main_wrapper:after{width: 75% !important;}</style>');

})


$('#last_phase').click(function(e){
    e.preventDefault();

    $(".question_7").fadeOut(300);
    $(".last_phase").delay(300).fadeIn();

    $('head').append('<style>.question_main_wrapper:after{width: 100% !important;}</style>');

})

$('#start_over').click(function(e){
    e.preventDefault();

    $(".last_phase").fadeOut(300);
    $(".question_1").delay(300).fadeIn();

    $('head').append('<style>.question_main_wrapper:after{width: 12.5% !important;}</style>');

})

$('#yes_i_do').click(function(e){
    e.preventDefault();

    $(".first_step").fadeOut(300);
    $(".second_step").delay(300).fadeIn();

})

$('button#choose_dest_account').click(function(e){
    e.preventDefault();

    $(".second_step").fadeOut(300);
    $(".third_step").delay(300).fadeIn();

})

// My account page

$(".acct_stages > div").click(function(){

    $(".acct_stages div.active").removeClass("active");
    $(this).addClass("active");

})


$('#acct_sett').click(function(e){
    e.preventDefault();

    $("[id*='account_sett_']").not("#account_sett_acct").fadeOut(300);

    $("#account_sett_acct").delay(300).fadeIn();  
})

$('#acct_pass').click(function(e){
    e.preventDefault();

    $("[id*='account_sett_']").not("#account_sett_change_pass").fadeOut(300);

    $("#account_sett_change_pass").delay(300).fadeIn();  
})

$('#acct_docs').click(function(e){
    e.preventDefault();

    $("[id*='account_sett_']").not("#account_sett_docs").fadeOut(300);

    $("#account_sett_docs").delay(300).fadeIn();  
})

$('#acct_kin').click(function(e){
    e.preventDefault();

    $("[id*='account_sett_']").not("#account_sett_kin").fadeOut(300);

    $("#account_sett_kin").delay(300).fadeIn();  
})

$('#acct_emp').click(function(e){
    e.preventDefault();

    $("[id*='account_sett_']").not("#account_sett_emp").fadeOut(300);

    $("#account_sett_emp").delay(300).fadeIn();  
})

$('#acct_inv').click(function(e){
    e.preventDefault();

    $("[id*='account_sett_']").not("#account_sett_inv").fadeOut(300);

    $("#account_sett_inv").delay(300).fadeIn();  
})







if(window.innerWidth <= 900) {

    var scroll_width = 100 * $(".acct_stages > div").length;
    $(".acct_stages").css("width", scroll_width+"px");

}




// New


$('#goto_settins_address').click(function(e){
    e.preventDefault();
    
    $(".acct_stages div.active").not('.acct_stages #acct_pass').removeClass("active");
    $('.acct_stages #acct_pass').addClass("active");


    $("#account_sett_acct").fadeOut(300);

    $("#account_sett_change_pass").delay(300).fadeIn();  
})

$('#goto_settins_documents').click(function(e){
    e.preventDefault();

    $(".acct_stages div.active").not('.acct_stages #acct_docs').removeClass("active");
    $('.acct_stages #acct_docs').addClass("active");


    $("#account_sett_change_pass").fadeOut(300);

    $("#account_sett_docs").delay(300).fadeIn();  
})

$('#goto_settins_kin').click(function(e){
    e.preventDefault();

    $(".acct_stages div.active").not('.acct_stages #acct_kin').removeClass("active");
    $('.acct_stages #acct_kin').addClass("active");


    $("#account_sett_docs").fadeOut(300);

    $("#account_sett_kin").delay(300).fadeIn();  
}) 


$(".question_1 .question_month_options_div div").click(function(){

    $(".question_1 .question_month_options_div div.active").removeClass("active");
    $(this).addClass("active"); 

})

$(".question_2 .question_month_options_div div").click(function(){

    $(".question_2 .question_month_options_div div.active").removeClass("active");
    $(this).addClass("active"); 

})

$(".question_3 .question_month_options_div div").click(function(){

    $(".question_3 .question_month_options_div div.active").removeClass("active");
    $(this).addClass("active"); 

})

$(".question_4 .question_month_options_div div").click(function(){

    $(".question_4 .question_month_options_div div.active").removeClass("active");
    $(this).addClass("active"); 

})


$(".question_5 .question_month_options_div div").click(function(){

    $(".question_5 .question_month_options_div div.active").removeClass("active");
    $(this).addClass("active"); 

})

$(".question_6 .question_month_options_div div").click(function(){

    $(".question_6 .question_month_options_div div.active").removeClass("active");
    $(this).addClass("active"); 

})

$(".question_7 .question_month_options_div div").click(function(){

    $(".question_7 .question_month_options_div div.active").removeClass("active");
    $(this).addClass("active"); 

})

// 




// End of my account page






// Chart js

var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
// The type of chart we want to create
type: 'line',

// The data for our dataset
data: {
labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
datasets: [{
label: 'My First dataset',
backgroundColor: 'rgba(0, 0, 0,0)',
borderColor: 'rgb(16, 192, 109)',
data: [45, 30, 20, 5, 2, 10, 0]
}]
},

// Configuration options go here
options: {}
});

//  End of Chart







})


// Get Screen size for responsiveness


var w = window.innerWidth;
var h = window.innerHeight;
console.log(`

    The Screen width is : ${w}px
    The Screen height is : ${h}px

`)


// End of Get Screen size for responsiveness
